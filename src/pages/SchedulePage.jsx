import React from 'react'
import ScheduleList from '../components/schedule/ScheduleList'

function SchedulePage() {
  return (
    <div>
      <ScheduleList/>
    </div>
  )
}

export default SchedulePage