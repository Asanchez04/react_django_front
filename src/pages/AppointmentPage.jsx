import React from 'react'
import AppointmentList from '../components/appointment/AppointmentList'

function AppointmentPage() {
  return (
    <div>
        <AppointmentList/>
    </div>
  )
}

export default AppointmentPage