import React from "react";
import { Field, Form, Formik } from "formik";
import { getToken } from "../utils/getToken";
import { useNavigate } from "react-router-dom";

function Login({ innerRef }) {
  
  let nav = useNavigate()
  const data_user = {
    username: "",
    password: "",
  };

  const send_data = (loginData, actions) => {
    actions.setSubmitting(true);
    getToken(loginData);
    actions.resetForm();
    actions.setSubmitting(false);
    nav("/");
  };

  return (
      
    <Formik initialValues={data_user} onSubmit={send_data} innerRef={innerRef}>
      {({}) => {
        return (
          <Form>
            <div
              className="form-group p-3 container"
              style={{
                borderColor: "green",
                borderRadius: "10px",
                backgroundColor: "Highlight",
                opacity: "0.9"
              }}
            >
              <label htmlFor="username">UserName</label>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="username"
                placeholder="Ingrese su nombre de usuario"
              />

              <label htmlFor="pass">Password</label>
              <Field
                className="form-control"
                id="pass"
                type="password"
                name="password"
                placeholder="Ingrese su contraseña"
              />
              <button className="btn btn-primary" type="submit">Login</button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default Login;
