import React from 'react'
import PatientList from '../components/patient/PatientList'

function PatientPage() {
  return (
    <div>
      <PatientList/>
    </div>
  )
}

export default PatientPage