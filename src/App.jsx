import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import DoctorPage from './pages/DoctorPage';
import Login from './pages/Login';
import PrivateRoute from './utils/route_active';
import Navbar from './components/navbar/navbar';
import DoctorRegister from './components/doctor/DoctorRegister';
import PatientRegister from './components/patient/PatientRegister';
import PatientPage from './pages/PatientPage';
import DoctorUpdate from './components/doctor/DoctorUpdate';
import PatientUpdate from './components/patient/PatientUpdate';
import SchedulePage from './pages/SchedulePage';
import ScheduleRegister from './components/schedule/ScheduleRegister';
import ScheduleUpdate from './components/schedule/ScheduleUpdate';
import AppointmentPage from './pages/AppointmentPage';
import AppointmentRegister from './components/appointment/AppointmentRegister';
import AppointmentUpdate from './components/appointment/AppointmentUpdate';
import {
  Routes,
  Route,
} from 'react-router-dom';


function App() {
  const [count, setCount] = useState(0)

  return (
    <div className='App'>
      <Navbar/>
      <Header/>
      <Routes>
        
          <Route path="/login/" element={<Login/>} />
          

          <Route path="doctors/" element={<PrivateRoute />}>
              <Route path="" element={<DoctorPage/>}/>
              <Route path="add/" element={<DoctorRegister/>}/>
              <Route path="update/:id/" element={<DoctorUpdate/>} />
          </Route>

          <Route path="patients/" element={<PrivateRoute />}>
              <Route path="" element={<PatientPage/>}/>
              <Route path="add/" element={<PatientRegister/>}/>
              <Route path="update/:id/" element={<PatientUpdate/>} />
          </Route>

          <Route path="schedules/" element={<PrivateRoute />}>
              <Route path="" element={<SchedulePage/>}/>
              <Route path="add/" element={<ScheduleRegister/>}/>
              <Route path="update/:id/" element={<ScheduleUpdate/>} />
          </Route>

          <Route path="appointments/" element={<PrivateRoute />}>
              <Route path="" element={<AppointmentPage/>}/>
              <Route path="add/" element={<AppointmentRegister/>}/>
              <Route path="update/:id/" element={<AppointmentUpdate/>} />
          </Route>
        
        
      </Routes>  
    </div>
  )
}

export default App
