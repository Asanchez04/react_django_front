import axios_instance from "../axios"

export function Update(values, url) {
    axios_instance({
        url: url, method: 'put', data: values
    }).then((response) => {
        if (response.statusText === "OK") {
            return response
        }
    }).catch((e) => {
        let e_response = e.response
        return e_response
    })

}