import axios_instance from "../axios"

export function Register(url, values) {

    axios_instance({
        url: url, method: 'post', data: values
    }).then((response) => {
        if (response.statusText === "OK") {
            return response
        }
    }).catch((e) => {
        let e_response = e.response
        return e_response
    })

}