import axios_instance from "../axios"

export function Delete(url) {
    axios_instance({
        url: url, method: 'delete'
    }).then((response) => {
        if (response.statusText === "OK") {
            console.log(response)
            return response
        }
    }).catch((e) => {
        let e_response = e.response
        return e_response
    })
    window.location.reload()
}