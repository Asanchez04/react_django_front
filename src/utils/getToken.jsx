import axios_instance from "./axios"

export function getToken(dataUser) {

    axios_instance({
        url: 'token/', method: 'post', data: {
            "username": dataUser.username,
            "password": dataUser.password,
        }
    }).then((response) => {
        localStorage.setItem('tokenRefresh', JSON.stringify(response.data.refresh))
        localStorage.setItem('tokenAccess', JSON.stringify(response.data.access))
        localStorage.setItem('user', dataUser.username)
    }).catch((except) => {
        let e_response = except.response
        localStorage.clear()
        return e_response
    })

}