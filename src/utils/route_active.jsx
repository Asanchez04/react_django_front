import {
    Outlet,
    useNavigate,
    useLocation,
    Navigate,
  } from 'react-router-dom';

const PrivateRoute = () => {
    const nav = useNavigate();
    const location = useLocation();
    const canAccess = !!localStorage.getItem("tokenAccess");
    return (
      <div>
  
        <button
          className="btn btn-primary"
          onClick={() => {
            localStorage.removeItem("tokenAccess");
            localStorage.removeItem("user");
            nav("/login/");
          }}
        >
          Logout
        </button>
  
        {canAccess ? (
          <Outlet />
        ) : (
          <Navigate to="/login/" replace state={{ url: location.pathname }} />
        )}
      </div>
    );
  };

export default PrivateRoute;