import axios from 'axios'

let TOKEN = JSON.parse(localStorage.getItem('tokenAccess'))


const axios_instance = axios.create({
    baseURL: 'http://127.0.0.1:8000/apis/',
    headers: {
        'Authorization': `Bearer ${TOKEN}`,
        'Content-Type': 'application/json',
        'accept': 'application/json',
    }
});

export default axios_instance;