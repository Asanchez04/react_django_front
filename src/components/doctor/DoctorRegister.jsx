import { Field, Form, Formik } from 'formik';
import React from 'react';
import { Register } from '../../utils/utilModels/register';
import { useNavigate } from 'react-router-dom';

function DoctorRegister({innerRef}) {
  const nav = useNavigate()
  let url = 'doctor/'

    let user = localStorage.getItem('user')
    const initial_data = {
        first_name: "",
        last_name: "",
        email: "",
        address: "",
        DNI: "",
        cell: "",
        gender: "Masculino",
        school_number: "",
        date_of_date: "",
        user_register: user,
        user_update: user,
    }

    const submit = (Values, actions) => {
        actions.setSubmitting(true);
        console.log(url, Values)
        actions.resetForm();
        Register(url, Values);
        actions.setSubmitting(false);
        nav('doctors/')
    };

    return (
        <Formik
            initialValues={initial_data}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                    <Form>
                        <div
                            className="form-group p-3 container"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >
                            <h2 className='text-center'>Doctor Register</h2>
                            <label htmlFor="first_name">Firts Name</label>
                            <Field
                                className="form-control"
                                id="first_name"
                                type="text"
                                name="first_name"
                                placeholder="Insert doctor's name"
                                required
                            />

                            <label htmlFor="last_name">Last Name</label>
                            <Field
                                className="form-control"
                                id="last_name"
                                type="text"
                                name="last_name"
                                placeholder="Insert doctor's last name"
                                required
                            />

                            <label htmlFor="DNI">DNI</label>
                            <Field
                                className="form-control"
                                id="DNI"
                                type="text"
                                name="DNI"
                                placeholder="Insert doctor's dni"
                                required
                            />

                            <label htmlFor="address">Address</label>
                            <Field
                                className="form-control"
                                id="address"
                                type="text"
                                name="address"
                                placeholder="Insert doctor's address"
                                required
                            />

                            <label htmlFor="email">Email</label>
                            <Field
                                className="form-control"
                                id="email"
                                type="email"
                                name="email"
                                placeholder="Insert doctor's email"
                                required
                            />

                            <label htmlFor="cell">Phone</label>
                            <Field
                                className="form-control"
                                id="cell"
                                type="text"
                                name="cell"
                                placeholder="Insert doctor's name phone"
                                required
                            />
                            
                            <label htmlFor="gender">Gender</label>
                            <br/>
                            <Field name="gender" as="select" def>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino">Masculino</option>
                            </Field>
                            
                            <br/>
                            
                            <label htmlFor="school_number">School Number</label>
                            <Field
                                className="form-control"
                                id="school_number"
                                type="text"
                                name="school_number"
                                placeholder="Insert doctor's school number"
                                required
                            />

                            <label htmlFor="date_of_date">BirhDay</label>
                            <Field
                                className="form-control"
                                id="date_of_date"
                                type="date"
                                name="date_of_date"
                                placeholder="Insert doctor's birthday"
                                required
                            />
                            <br/>
                            <button className="btn btn-primary" type="submit">Agregar</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}

export default DoctorRegister