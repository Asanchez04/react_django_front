import React from "react";
import { Field, Form, Formik } from "formik";
import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios_instance from "../../utils/axios";
import { Update } from "../../utils/utilModels/update";

function DoctorUpdate({ innerRef }) {
  const [doctor, setDoctor] = useState(null);
  const params = useParams();
  let login_user = localStorage.getItem("user");
  const nav = useNavigate()

  let initial_data = {
    first_name: doctor?.first_name,
    last_name: doctor?.last_name,
    email: doctor?.email,
    address: doctor?.address,
    DNI: doctor?.DNI,
    cell: doctor?.cell,
    gender: doctor?.gender,
    school_number: doctor?.school_number,
    date_of_date: doctor?.date_of_date,
    user_register: doctor?.user_register,
    user_update: login_user,
    is_active: doctor?.is_active,
  };

  let url = `doctor/${params.id}/`;
  console.log(params.id);
  console.log(doctor);

  useEffect(() => {
    axios_instance(url).then((response) => {
      setDoctor(response.data);
    });
  }, []);

 

  const submit = (values, actions) => {
    actions.setSubmitting(true);

    delete values.id;

    values.user_update = login_user;
    console.log(values);
    Update(values, url);
    //actions.resetForm();
    actions.setSubmitting(false);
    nav(-1);
  };

  return (
    <div>
      {doctor ? (
        <Formik
          initialValues={initial_data}
          onSubmit={submit}
          innerRef={innerRef}
        >
          {({}) => {
            return (
              <Form>
                <div
                  className="form-group p-3 container"
                  style={{
                    outline: "1px solid blue",
                    borderRadius: "10px",
                  }}
                >
                  <h2 className='text-center'>Doctor Update</h2>
                  <label htmlFor="first_name">First Name</label>
                  <Field
                    className="form-control"
                    id="first_name"
                    type="text"
                    name="first_name"
                    placeholder="Ingrese el nombre del medico."
                    required
                  />

                  <label htmlFor="last_name">Last Name</label>
                  <Field
                    className="form-control"
                    id="last_name"
                    type="text"
                    name="last_name"
                    placeholder="Ingrese el apellido del medico."
                    required
                  />

                  <label htmlFor="DNI">DNI</label>
                  <Field
                    className="form-control"
                    id="DNI"
                    type="text"
                    name="DNI"
                    placeholder="Ingrese el DNI del medico."
                    required
                  />

                  <label htmlFor="address">Address</label>
                  <Field
                    className="form-control"
                    id="address"
                    type="text"
                    name="address"
                    placeholder="Ingrese la direcion del medico."
                    required
                  />

                  <label htmlFor="email">Email</label>
                  <Field
                    className="form-control"
                    id="email"
                    type="email"
                    name="email"
                    placeholder="Ingrese el correo electronico del medico."
                    required
                  />

                  <label htmlFor="cell">Phone</label>
                  <Field
                    className="form-control"
                    id="cell"
                    type="text"
                    name="cell"
                    placeholder="Ingrese el numero de elefono del medico."
                    required
                  />

                  <label htmlFor="gender">Gender</label>
                  <Field name="gender" as="select" def>
                    <option value="Femenino">Femenino</option>
                    <option value="Masculino">Masculino</option>
                  </Field>

                  <label htmlFor="school_number">School Number</label>
                  <Field
                    className="form-control"
                    id="school_number"
                    type="text"
                    name="school_number"
                    placeholder="Ingrese el num. de colegiatura del medico."
                    required
                  />

                  <label htmlFor="date_of_date">BirthDay</label>
                  <Field
                    className="form-control"
                    id="date_of_date"
                    type="date"
                    name="date_of_date"
                    placeholder="Ingrese la fecha de naciemiento del medico."
                    required
                  />

                  <button className="btn btn-primary" type="submit">Update</button>
                </div>
              </Form>
            );
          }}
        </Formik>
      ) : (
        <h1>Loading Form...</h1>
      )}
    </div>
  );
}

export default DoctorUpdate;
