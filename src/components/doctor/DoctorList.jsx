import React, { useState, useEffect } from "react";
import DoctorItem from "./DoctorItem";
import axios_instance from "../../utils/axios";

function DoctorList() {
  let [doctors, setDoctors] = useState([]);

  useEffect(() => {
    getDoctors();
  }, []);


  async function getDoctors() {
    axios_instance({
      url: 'doctor/', method: 'get'
    }).then((response)=>{
      console.log(response)
      setDoctors(response.data)
    })
  }

  return (
    <div className="container">
      <h2 className="text-center">Doctors</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Gender</th>
            <th scope="col">Email</th>
            <th scope="col">Adreess</th>
            <th scope="col">User Register</th>
            <th className="text-center" scope="col">Options</th>
          </tr>
        </thead>
        {doctors.results?.map((element) => {
          return <DoctorItem key={element.id} {...element} />;
        })}
      </table>
    </div>
  );
}

export default DoctorList;
