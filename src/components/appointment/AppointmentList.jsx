import React, { useState, useEffect } from "react";
import AppointmentItem from "./AppointmentItem";
import axios_instance from "../../utils/axios";

function AppointmentList() {
  let [appointments, setAppointments] = useState([]);

  useEffect(() => {
    getAppointments();
  }, []);


  async function getAppointments() {
    axios_instance({
      url: 'appointment/', method: 'get'
    }).then((response)=>{
      console.log(response)
      setAppointments(response.data)
    })
  }

{/* <th scope="row">{id}</th>
        <td>{doctor_id}</td>
        <td>{patient_id}</td>
        <td>{date_attention}</td>
        <td>{start_attention}</td>
        <td>{finish_attention}</td>
        <td>{state}</td>
        <td>{observations}</td>
        <td>{user_register}</td> */}



  return (
    <div className="">
      <h2 className="text-center">Appointments</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Doctor</th>
            <th scope="col">Patient</th>
            <th scope="col">Date Attention</th>
            <th scope="col">Start Attention</th>
            <th scope="col">Finish Attention</th>
            <th scope="col">State</th>
            <th scope="col">Observations</th>
            <th scope="col">User Register</th>
            <th scope="col">Status</th>
            <th className="text-center" scope="col">Options</th>
          </tr>
        </thead>
        {appointments.results?.map((element) => {
          return <AppointmentItem key={element.id} {...element} />;
        })}
      </table>
    </div>
  );
}

export default AppointmentList;