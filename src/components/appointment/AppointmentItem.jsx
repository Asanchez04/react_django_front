import React from "react";
import { Delete } from "../../utils/utilModels/delete";


function AppointmentItem({
  id,
  doctor_id,
  patient_id,
  date_attention,
  start_attention,
  finish_attention,
  state,
  observations,
  user_register,
  is_active,
}) {

    

  const url = `appointment/${id}`
  const url_update = `/appointments/update/${id}`
  function onClick() {
    Delete(url)
  }

  
  return (
    <tbody>
      <tr>
        <th scope="row">{id}</th>
        <td>{doctor_id}</td>
        <td>{patient_id}</td>
        <td>{date_attention}</td>
        <td>{start_attention}</td>
        <td>{finish_attention}</td>
        <td>{state}</td>
        <td>{observations}</td>
        <td>{user_register}</td>
        {is_active ? <td>Is Active</td> : <td>Isn't Active</td> }
        <td className="text-center">
            <a className="btn btn-primary" style={{marginRight: "5px" }} href={url_update} >Update</a>
            <button className="btn btn-danger" onClick={()=>{
              onClick()
            }} >Delete</button>
        </td>
      </tr>
    </tbody>
  );
}

export default AppointmentItem;