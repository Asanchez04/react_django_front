import { Field, Form, Formik } from 'formik';
import React, { useState, useEffect } from "react";
import { Register } from '../../utils/utilModels/register';
import { useNavigate } from 'react-router-dom';
import axios_instance from '../../utils/axios';

function AppointmentRegister({innerRef}) {
  const nav = useNavigate()
  let [doctors, setDoctors] = useState([]);
  let [patients, setPatients] = useState([]);

  useEffect(() => {
    getDoctors();
    getPatients();
  }, []);


  async function getDoctors() {
    axios_instance({
      url: 'doctor/', method: 'get'
    }).then((response)=>{
      console.log(response.data)
      setDoctors(response.data)
    })
  }
  async function getPatients() {
    axios_instance({
      url: 'patient/', method: 'get'
    }).then((response)=>{
      console.log(response.data)
      setPatients(response.data)
    })
  }
    const url = 'appointment/'

    let user = localStorage.getItem('user')
    const initial_data = {
      doctor_id: "",
      patient_id: "",
      date_attention: "",
      state: "",
      observations: "Sin observaciones aun",
      user_register: user,
      user_update: user,
    }

    const submit = (Values, actions) => {
        actions.setSubmitting(true);
        console.log(Values)
        actions.resetForm();
        Register(url, Values);
        actions.setSubmitting(false);
        nav('/appointments')
    };

    return (
      <div>
        {doctors && patients ? <Formik 
            initialValues={initial_data}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                  
                    <Form>
                        <div
                            className="form-group p-3 container"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >   
                            <h2 className='text-center'> Appointment Register</h2>
                            <label htmlFor="doctor_id">Doctor</label>
                            <br></br>    
                            <Field required name="doctor_id" as="select" def>
                              <option value={null} ></option>
                              {doctors.results?.map((element) => {
                                return <option key={element.id} value={element.id}>{`${element.first_name}`}</option>
                              })}
                            </Field> 

                            
                            <br/>   
                            <label htmlFor="patient_id">Patient</label>
                            <br/>     
                            <Field required name="patient_id" as="select" def>
                              <option value={null} ></option>
                              {patients.results?.map((element) => {
                                return <option key={element.id} value={element.id}>{element.first_name}</option>
                              })}
                            </Field>  

                            <br/>      
                            <label htmlFor="date_attention">Date Attention</label>
                            <Field
                                className="form-control"
                                id="date_attention"
                                type="date"
                                name="date_attention"
                                placeholder="Ingrese el apellido del medico."
                                required
                            />

                            <label htmlFor="observations">Observations</label>
                            <Field
                                className="form-control"
                                id="observations"
                                type="text"
                                name="observations"
                                placeholder="Observations"
                                required
                            />
                            
                            <label htmlFor="state">State</label>
                            <br/> 
                            <Field required name="state" as="select" def>
                                <option value={null} ></option>
                                <option value="pending">Pending</option>
                                <option value="in progress">In Progress</option>
                                <option value="finished">Finished</option>
                            </Field>

                            <br/>   
                            <br/>   
                            <button className="btn btn-primary" type="submit">Agregar</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>: <h2>Loading Form...</h2>}
        </div>
    );
}

export default AppointmentRegister