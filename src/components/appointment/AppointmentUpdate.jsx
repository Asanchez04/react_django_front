import React from "react";
import { Field, Form, Formik } from "formik";
import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios_instance from "../../utils/axios";
import { Update } from "../../utils/utilModels/update";

function AppointmentUpdate({ innerRef }) {
  const [appointment, setAppointment] = useState(null);
  const [doctor, setDoctor] = useState(null);
  const [doctors, setDoctors] = useState(null);
  const [patient, setPatient] = useState(null)
  const [patients, setPatients] = useState(null)

  const params = useParams();
  let login_user = localStorage.getItem("user");
  const nav = useNavigate()

  let initial_data = {
    doctor_id: appointment?.doctor_id,
    patient_id: appointment?.patient_id,
    date_attention: appointment?.date_attention,
    state: appointment?.state,
    observations: appointment?.observations,
    user_register: appointment?.user_register,
    user_update: login_user,
    is_active: appointment?.is_active,
  };

  let url = `appointment/${params.id}/`;
  let url_doctor = `doctor/${appointment?.doctor_id}`
  let url_patient = `patient/${appointment?.patient_id}`
  console.log(params.id);
  console.log(appointment);

  useEffect(() => {
    axios_instance(url).then((response) => {
      setAppointment(response.data);
    });

    axios_instance('doctor/').then((res) => {
      setDoctors(res.data)
    })

    { appointment ?
      axios_instance(url_doctor).then((response) => {
        setDoctor(response.data);
      }) : ''
  
    }
    { appointment ?
      axios_instance(url_patient).then((response) => {
        setPatient(response.data);
      }) : ''
  
    }

    axios_instance('patient/').then((res) => {
      setPatients(res.data)
    })

  }, []);

  const submit = (values, actions) => {
    actions.setSubmitting(true);

    delete values.id;

    values.user_update = login_user;
    console.log(values);
    Update(values, url);
    //actions.resetForm();
    actions.setSubmitting(false);
    nav(-1);
  };

  return (
    <div>
      {appointment ? 
        <Formik
          initialValues={initial_data}
          onSubmit={submit}
          innerRef={innerRef}
        >
          {({}) => {
            return (
              <Form>
                        <div
                            className="form-group p-3 container"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >   
                            <h2 className='text-center'> Appointment Update</h2>
                            <label htmlFor="doctor_id">Doctor</label>
                            <br></br>  


                            <Field required name="doctor_id" as="select" def>
                            <option value="default" disabled>Choose a Doctor</option>
                              {
                                doctor ? <option value={appointment.doctor_id}>{doctor.first_name}</option> : ''
                              }
                              {doctors ? doctors.results?.map((element) => {
                                return <option key={element.id} value={element.id}>{`${element.first_name}`}</option>
                              }): 'loading...'}
                            </Field> 

                            
                            <br/>   
                            <label htmlFor="patient_id">Patient</label>
                            <br/>     
                            <Field required name="patient_id" as="select" def>
                              
                              <option value="default" disabled>Choose a Patient</option>
                              {/* {
                                patient ? <option value={appointment.patient_id}>{patient.first_name}</option> : ''
                              } */}
                              {patients ? patients.results?.map((element) => {
                                return <option key={element.id} value={element.id}>{`${element.first_name}`}</option>
                              }): 'loading...'}
                            </Field>  

                            <br/>      
                            <label htmlFor="date_attention">Date Attention</label>
                            <Field
                                className="form-control"
                                id="date_attention"
                                type="date"
                                name="date_attention"
                                placeholder="Ingrese el apellido del medico."
                                required
                            />

                            <label htmlFor="observations">Observations</label>
                            <Field
                                className="form-control"
                                id="observations"
                                type="text"
                                name="observations"
                                placeholder="Observations"
                                required
                            />
                            
                            <label htmlFor="state">State</label>
                            <br/> 
                            <Field required name="state" as="select" def>
                                <option value={null} ></option>
                                <option value="pending">Pending</option>
                                <option value="in progress">In Progress</option>
                                <option value="finished">Finished</option>
                            </Field>

                            <br></br> 
                            <label htmlFor="is_active">Status</label>
                            <br></br> 
                                <Field name="is_active" as="select">
                                    <option value={true}>Is Active</option>
                                    <option value={false}>Isn't Active</option>
                                </Field>
                                <br></br> 
                            <button className="btn btn-primary" type="submit">Update</button>
                        </div>
                    </Form>
            );
          }}
        </Formik> : <h2>Loading Form...</h2>
      }
    </div>
  );
}

export default AppointmentUpdate;