import React from "react";

function Dropdown({ name, list, create }) {
  return (
    <li className="nav-item dropdown">
      <a
        className="nav-link dropdown-toggle"
        href={list}
        id="navbarDropdown"
        role="button"
        data-bs-toggle="dropdown"
        aria-expanded="false"
      >
        {name}
      </a>
      <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
        <li>
          <a className="dropdown-item" href={create}>
            New {name}
          </a>
        </li>
        <li>
          <hr className="dropdown-divider" />
        </li>

        <li>
          <a className="dropdown-item" href={list}>
            List {name}
          </a>
        </li>
        <li>
          <hr className="dropdown-divider" />
        </li>
      </ul>
    </li>
  );
}

export default Dropdown;
