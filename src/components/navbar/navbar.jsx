import React from 'react'
import Dropdown from './dropdown';

function Navbar() {
  return (
    <div>
            <nav className="navbar navbar-expand-lg bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" href="/">REACT WITH DJANGO</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            
                            <Dropdown name={"Doctor"} list="/doctors/" create="/doctors/add/"  />
                            <Dropdown name={"Patient"} list="/patients/" create="/patients/add/"  />
                            <Dropdown name={"Schedule"} list="/schedules/" create="/schedules/add/"/>
                            <Dropdown name={"Appointment"} list="/appointments/" create="/appointments/add/"/>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
  )
}

export default Navbar;