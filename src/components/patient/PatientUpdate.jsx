import React from "react";
import { Field, Form, Formik } from "formik";
import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios_instance from "../../utils/axios";
import { Update } from "../../utils/utilModels/update";

function PatientUpdate({ innerRef }) {
  const [patient, setPatient] = useState(null);
  const params = useParams();
  let login_user = localStorage.getItem("user");
  const nav = useNavigate()

  let initial_data = {
    first_name: patient?.first_name,
    last_name: patient?.last_name,
    address: patient?.address,
    DNI: patient?.DNI,
    cell: patient?.cell,
    gender: patient?.gender,
    date_of_date: patient?.date_of_date,
    user_register: patient?.user_register,
    user_update: login_user,
    is_active: patient?.is_active,
  };

  let url = `patient/${params.id}/`;
  console.log(params.id);
  console.log(patient);

  useEffect(() => {
    axios_instance(url).then((response) => {
      setPatient(response.data);
    });
  }, []);

 

  const submit = (values, actions) => {
    actions.setSubmitting(true);

    delete values.id;

    values.user_update = login_user;
    console.log(values);
    Update(values, url);
    //actions.resetForm();
    actions.setSubmitting(false);
    nav(-1);
  };

  return (
    <div>
      {patient ? (
        <Formik
          initialValues={initial_data}
          onSubmit={submit}
          innerRef={innerRef}
        >
          {({}) => {
            return (
              <Form>
                        <div
                            className="form-group p-3 container"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >
                            <h2 className='text-center'>Patient Update</h2>
                            <label htmlFor="first_name">Firts Name</label>
                            <Field
                                className="form-control"
                                id="first_name"
                                type="text"
                                name="first_name"
                                placeholder="Insert patient's name"
                                required
                            />

                            <label htmlFor="last_name">Last Name</label>
                            <Field
                                className="form-control"
                                id="last_name"
                                type="text"
                                name="last_name"
                                placeholder="Insert patient's last name"
                                required
                            />

                            <label htmlFor="DNI">DNI</label>
                            <Field
                                className="form-control"
                                id="DNI"
                                type="text"
                                name="DNI"
                                placeholder="Insert patient's dni"
                                required
                            />

                            <label htmlFor="address">Address</label>
                            <Field
                                className="form-control"
                                id="address"
                                type="text"
                                name="address"
                                placeholder="Insert patient's address"
                                required
                            />

                            <label htmlFor="cell">Phone</label>
                            <Field
                                className="form-control"
                                id="cell"
                                type="text"
                                name="cell"
                                placeholder="Insert patient's phone"
                                required
                            />

                            <label htmlFor="gender">Gender</label>
                            <br/>
                            <Field name="gender" as="select" def>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino">Masculino</option>
                            </Field>

                            <br/>
                            <label htmlFor="date_of_date">BirthDay</label>
                            <Field
                                className="form-control"
                                id="date_of_date"
                                type="date"
                                name="date_of_date"
                                placeholder="Insert patient's birthday"
                                required
                            />
                            <br/>
                            <button className="btn btn-primary" type="submit">Update</button>
                        </div>
                    </Form>
            );
          }}
        </Formik>
      ) : (
        <h1>Loading Form...</h1>
      )}
    </div>
  );
}

export default PatientUpdate;
