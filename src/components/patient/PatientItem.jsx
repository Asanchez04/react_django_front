import React from "react";
import { Delete } from "../../utils/utilModels/delete";


function PatientItem({
  id,
  first_name,
  last_name,
  gender,
  email,
  address,
  user_register,
}) {

  const url = `patient/${id}`
  const url_update = `/patients/update/${id}`
  function onClick() {
    Delete(url)
  }

  
  return (
    <tbody>
      <tr>
        <th scope="row">{id}</th>
        <td>{first_name}</td>
        <td>{last_name}</td>
        <td>{gender}</td>
        <td>{address}</td>
        <td>{user_register}</td>
        <td className="text-center">
            <a className="btn btn-primary" style={{marginRight: "5px" }} href={url_update} >Update</a>
            <button className="btn btn-danger" onClick={()=>{
              onClick()
            }} >Delete</button>
        </td>
      </tr>
    </tbody>
  );
}

export default PatientItem;
