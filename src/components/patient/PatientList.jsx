import React, { useState, useEffect } from "react";
import PatientItem from "./PatientItem";
import axios_instance from "../../utils/axios";

function PatientList() {
  let [patients, setPatients] = useState([]);

  useEffect(() => {
    getpatients();
  }, []);


  async function getpatients() {
    axios_instance({
      url: 'patient/', method: 'get'
    }).then((response)=>{
      console.log(response)
      setPatients(response.data)
    })
  }

  return (
    <div className="container">
      <h2 className="text-center">Patients</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Gender</th>
            <th scope="col">Adreess</th>
            <th scope="col">User Register</th>
            <th className="text-center" scope="col">Options</th>
          </tr>
        </thead>
        {patients.results?.map((element) => {
          return <PatientItem key={element.id} {...element} />;
        })}
      </table>
    </div>
  );
}

export default PatientList;