import React, { useState, useEffect } from "react";
import ScheduleItem from "./ScheduleItem";
import axios_instance from "../../utils/axios";

function ScheduleList() {
  let [schedules, setSchedules] = useState([]);

  useEffect(() => {
    getSchedules();
  }, []);


  async function getSchedules() {
    axios_instance({
      url: 'schedule/', method: 'get'
    }).then((response)=>{
      console.log(response)
      setSchedules(response.data)
    })
  }

  return (
    <div className="container">
      <h2 className="text-center">Schedules</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Doctor</th>
            <th scope="col">Date Attention</th>
            <th scope="col">Start Attention</th>
            <th scope="col">Finish Attention</th>
            <th scope="col">User Register</th>
            <th scope="col">Status</th>
            <th className="text-center" scope="col">Options</th>
          </tr>
        </thead>
        {schedules.results?.map((element) => {
          return <ScheduleItem key={element.id} {...element} />;
        })}
      </table>
    </div>
  );
}

export default ScheduleList;