import React from "react";
import { Field, Form, Formik } from "formik";
import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios_instance from "../../utils/axios";
import { Update } from "../../utils/utilModels/update";

function ScheduleUpdate({ innerRef }) {
  const [schedule, setSchedule] = useState(null);
  const [doctor, setDoctor] = useState(null);
  const [doctors, setDoctors] = useState(null);

  const params = useParams();
  let login_user = localStorage.getItem("user");
  const nav = useNavigate()

  let initial_data = {
    doctor_id: schedule?.doctor_id,
    date_attention: schedule?.date_attention,
    user_register: schedule?.user_register,
    user_update: login_user,
    is_active: schedule?.is_active,
  };

  let url = `schedule/${params.id}/`;
  let url_doctor = `doctor/${schedule?.doctor_id}`
  console.log(params.id);
  console.log(schedule);

  useEffect(() => {
    axios_instance(url).then((response) => {
      setSchedule(response.data);
    });

    axios_instance('doctor/').then((res) => {
      setDoctors(res.data)
    })

    { schedule ?
      axios_instance(url_doctor).then((response) => {
        setDoctor(response.data);
      }) : ''
  
    }
  }, []);

  const submit = (values, actions) => {
    actions.setSubmitting(true);

    delete values.id;

    values.user_update = login_user;
    console.log(values);
    Update(values, url);
    //actions.resetForm();
    actions.setSubmitting(false);
    nav(-1);
  };

  return (
    <div>
      {schedule ? 
        <Formik
          initialValues={initial_data}
          onSubmit={submit}
          innerRef={innerRef}
        >
          {({}) => {
            return (
              <Form>
                        <div
                            className="form-group p-3 container"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >   
                            <h2 className='text-center'> Schedule Update</h2>
                            <label htmlFor="date_attention">Doctor</label>
                            <br></br>    
                            <Field name="doctor_id" as="select" def>
                            <option value="default" disabled>Seleccione un doctor</option>
                            {
                             doctor ? <option value={schedule.doctor_id}>{doctor.first_name}</option> : ''
                            }
                            {doctors ? doctors?.results.map((doctor) => {
                                        return (
                                            <option key={doctor.id} value={doctor.id}>{doctor.first_name}</option>)
                            }) : "Loading..."
                            }
    
                            </Field>  

                            <br></br>    
                            <label htmlFor="date_attention">Date Attention</label>
                            <Field
                                className="form-control"
                                id="date_attention"
                                type="date"
                                name="date_attention"
                                required
                            />

                              <label htmlFor="is_active">Status</label>
                              <br></br> 
                                <Field name="is_active" as="select">
                                    <option value={true}>Is Active</option>
                                    <option value={false}>Isn't Active</option>
                                </Field>
                                <br></br> 
                            <button className="btn btn-primary" type="submit">Update</button>
                        </div>
                    </Form>
            );
          }}
        </Formik> : <h2>Loading Form...</h2>
      }
    </div>
  );
}

export default ScheduleUpdate;