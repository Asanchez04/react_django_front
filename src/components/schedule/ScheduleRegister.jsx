import { Field, Form, Formik } from 'formik';
import React, { useState, useEffect } from "react";
import { Register } from '../../utils/utilModels/register';
import { useNavigate } from 'react-router-dom';
import axios_instance from '../../utils/axios';

function ScheduleRegister({innerRef}) {
  const nav = useNavigate()
  let [doctors, setDoctors] = useState([]);

  useEffect(() => {
    getDoctors();
  }, []);


  async function getDoctors() {
    axios_instance({
      url: 'doctor/', method: 'get'
    }).then((response)=>{
      console.log(response.data)
      setDoctors(response.data)
    })
  }

  let url = 'schedule/'

    let user = localStorage.getItem('user')
    const initial_data = {
      doctor_id: "",
      date_attention: "",
      start_attention: "",
      finish_attention: "",
      user_register: user,
      user_update: user,
    }

    const submit = (Values, actions) => {
        actions.setSubmitting(true);
        console.log(url, Values)
        actions.resetForm();
        Register(url, Values);
        actions.setSubmitting(false);
        nav('/schedules')
    };

    return (
      <div>
        {doctors ? <Formik 
            initialValues={initial_data}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                  
                    <Form>
                        <div
                            className="form-group p-3 container"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >   
                            <h2 className='text-center'> Schedule Register</h2>
                            <label htmlFor="doctor_id">Doctor</label>
                            <br></br>    
                            <Field required name="doctor_id" as="select" def>
                              <option value={null} ></option>
                              {doctors.results?.map((element) => {
                                return <option key={element.id} value={element.id}>{element.first_name}</option>
                              })}
    
                            </Field>  

                            <br></br>    
                            <label htmlFor="date_attention">Date Attention</label>
                            <Field
                                className="form-control"
                                id="date_attention"
                                type="date"
                                name="date_attention"
                                placeholder="Ingrese el apellido del medico."
                                required
                            />

                            <label htmlFor="start_attention">Start Attention</label>
                            <Field
                                className="form-control"
                                id="start_attention"
                                type="datetime-local"
                                name="start_attention"
                                placeholder="Ingrese el DNI del medico."
                                required
                            />

                            <label htmlFor="finish_attention">Finish Attention</label>
                            <Field
                                className="form-control"
                                id="finish_attention"
                                type="datetime-local"
                                name="finish_attention"
                                placeholder="Ingrese la direcion del medico."
                                required
                            />
                            <br/>
                            <button className="btn btn-primary" type="submit">Agregar</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>: <h2>Loading Form...</h2>}
        </div>
    );
}

export default ScheduleRegister