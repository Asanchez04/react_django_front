import React from "react";
import { Delete } from "../../utils/utilModels/delete";


function ScheduleItem({
  id,
  doctor_id,
  date_attention,
  start_attention,
  finish_attention,
  user_register,
  is_active,
}) {

    

  const url = `schedule/${id}`
  const url_update = `/schedules/update/${id}`
  function onClick() {
    Delete(url)
  }

  
  return (
    <tbody>
      <tr>
        <th scope="row">{id}</th>
        <td>{doctor_id}</td>
        <td>{date_attention}</td>
        <td>{start_attention}</td>
        <td>{finish_attention}</td>
        <td>{user_register}</td>
        {is_active ? <td>Is Active</td> : <td>Isn't Active</td> }
        <td className="text-center">
            <a className="btn btn-primary" style={{marginRight: "5px" }} href={url_update} >Update</a>
            <button className="btn btn-danger" onClick={()=>{
              onClick()
            }} >Delete</button>
        </td>
      </tr>
    </tbody>
  );
}

export default ScheduleItem;